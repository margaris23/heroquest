package com.nima;

import java.util.ArrayList;
import java.util.List;

import com.nima.Dice.Side;

public abstract class Pawn {
	public enum STATE {
		GOOD, DEAD, CURSED, PETRIFIED
	}
	protected STATE currentState;
	
	protected int posx;	
	protected int posy;
	
	protected String name;
	protected int moveDice;
	
	protected int attackDice;
	protected int defendDice;
	protected int bodyPoints;
	protected int mindPoints;
	
	protected int availableMovePoints = 0; //initialize
	
	protected int owner;
	protected List<? extends Item> items = new ArrayList<Item>();
	
	protected void rollMovement(){
		RedDice rd = new RedDice();
		for(int i = 0; i <moveDice; i++){
			this.availableMovePoints += rd.roll().number(); 
		}
	}
	
	protected int rollAttackDefence(Side side, int num){
		int hits = 0;
		WhiteDice white = new WhiteDice();
		for(int i=0; i < num; i++){
			if(side == white.roll()){
				++hits;
			}			
		}		
		return hits;
	}
	
	protected int rollAttack(){
		return rollAttackDefence(Side.SKULL, getAttackDice());
	}
	
	protected int rollDefence(){
		return rollAttackDefence(Side.SHIELD, getDefendDice());
	}
	
	protected void hit(int loss){
		this.bodyPoints -= loss;
	}
	
	public STATE getCurrentState(){
		return currentState;
	}
	public String getName(){
		return name;
	}
	public int getMoveDice() {
		return moveDice;
	}
	public void setMoveDice(int moveDice) {
		this.moveDice = moveDice;
	}
	public int getAttackDice() {
		return attackDice;
	}
	public void setAttackDice(int attackDice) {
		this.attackDice = attackDice;
	}
	public int getDefendDice() {
		return defendDice;
	}
	public void setDefendDice(int defendDice) {
		this.defendDice = defendDice;
	}
	public int getBodyPoints() {
		return bodyPoints;
	}
	public void setBodyPoints(int bodyPoints) {
		this.bodyPoints = bodyPoints;
	}
	public int getMindPoints() {
		return mindPoints;
	}
	public void setMindPoints(int mindPoints) {
		this.mindPoints = mindPoints;
	}
	public void setCurrentState(STATE currentState) {
		this.currentState = currentState;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosx() {
		return posx;
	}
	public void setPosx(int posx) {
		this.posx = posx;
	}
	public int getPosy() {
		return posy;
	}
	public void setPosy(int posy) {
		this.posy = posy;
	}
	public int getMovePoints(){
		return availableMovePoints;
	}	
	public int getOwner() {
		return owner;
	}	
	public void setOwner(int owner) {
		this.owner = owner;
	}
	
	/*
	 * Abstract Section
	 */
	
	public abstract boolean move(int toX, int toY);
	public abstract boolean attack(Pawn other);
	public abstract boolean hasMoved();
	public abstract boolean actionPerformed();
}
