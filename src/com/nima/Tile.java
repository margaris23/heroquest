package com.nima;

public class Tile {
	public enum Type {
		UNUSED, EMPTY, BLOCKED, FURNITURE, TRAP, MONSTER, EXIT, ENTRY;
		public static Type fromChar(char c){
			switch(c){
			case 'u':
				return UNUSED;				
			case 'b':
				return BLOCKED;
			case 'e':
				return EMPTY;
			case 't':
				return TRAP;
			case 'f':
				return FURNITURE;
			case 'm':
				return MONSTER;
			case 'x':
				return EXIT;
			case 'n':
				return ENTRY;
			default:
				return EMPTY;		
			}
		}
		
		public char toChar(){
			switch(this){
				case UNUSED:
					return 'x';					
				case EMPTY:
					return '.';
				case BLOCKED:
					return 'B';
				case FURNITURE:
					return 'F';
				case TRAP:
					return 'T';
				case MONSTER:
					return 'M';
				case EXIT:
					return 'X';
				case ENTRY:
					return 'I';
				default:
					return '.';
			}
		}
	}
	
	//this object preserves the door-connection of a tile towards another
	private class Connection{
		private char direction;		
		private boolean hidden;
		
		public char getDirection(){
			return direction;
		}
		public boolean isHidden(){
			return hidden;
		}		
		
		public Connection(char direction, boolean hidden){
			this.direction = direction;
			this.hidden = hidden;
		}
	}
	
	public Tile(char c){
		this.type = Type.fromChar(c);
	}

	private Type type;
	private Connection connection;
	
	private int roomId;
	private Pawn pawn;
	private int door;
	
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(char direction,  boolean hidden) {
		this.connection = new Connection(direction, hidden);
	}
	public Type getType(){
		return type;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public Pawn getPawn() {
		return pawn;
	}
	public void setPawn(Pawn pawn) {
		this.pawn = pawn;
	}
	public void setDoor(int door) {
		this.door = door;
	}
	public int getDoor() {
		return door;
	}
	
}