package com.nima;

import java.util.ArrayList;
import java.util.List;

public class Board {
	
	/*
	 * Must:
	 * 		be able to provide room definitions 
	 */
	
	public static final int HMAX = 26;
	public static final int VMAX = 19;
	
	private Tile[][] tiles = new Tile[VMAX][HMAX];
	
	private List<Door> doors = new ArrayList<Door>();
	private List<? extends Pawn> pawns = new ArrayList<Pawn>();
	
	public void configure(char[] bitmap) throws Exception{
		if(bitmap.length != VMAX * HMAX){
			throw new Exception();
		}		
		int row = 0, col = 0;		
		for (char c : bitmap) {
			this.tiles[row][col++] = new Tile(c);
			if(col == HMAX){
				col = 0;
				++row;				
			}
		}
	}
	
	public void setupDoors(String doors_property){
		for(String door : doors_property.split(";")){
			String[] door_info = door.split(",");
			int col = Integer.parseInt(door_info[0]) - 1;
			int line = Integer.parseInt(door_info[1]) - 1;
			this.tiles[line][col].setConnection(door_info[2].charAt(0), "h".equals(door_info[3]));			
		}		
	}
	
	public String export(){
		StringBuilder sb = new StringBuilder();
		for(int row = 0; row < VMAX; row++){
			for(int col = 0; col < HMAX; col++){
				sb.append(tiles[row][col].getType().toChar());
			}
			sb.append("\n");
		}		
		return sb.toString();
	}
	
	public void createDoor(Door.ORIENTATION orientation, Door.TYPE type, Tile tile) {
		//TBD
	}
	
}