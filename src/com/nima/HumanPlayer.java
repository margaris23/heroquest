package com.nima;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class HumanPlayer extends Player{	
	private List<Hero> heroesOwned = new ArrayList<Hero>();	
	private int gold;
		
	private Object currentMovePoints = 0;
	private int rankPoints = 0;
	
	public HumanPlayer(String name){	
		this.name = name;
	}
	
	public HumanPlayer(Properties props){
		this.name = props.getProperty("name");
		this.gold = Integer.valueOf(props.getProperty("gold"));
		int heroes = Integer.valueOf(props.getProperty("heroes"));
		while(heroes > 0){			
			this.heroesOwned.add(new Hero("hero." + String.valueOf(heroes--) + ".", props));
			this.activePawn = this.heroesOwned.size() - 1;
		}
	}
	
	public void addHero(Hero hero){
		if(hero == null) return;
		this.heroesOwned.add(hero);
		this.activePawn = this.heroesOwned.size() - 1;
	}

	public Object getCurrentMovePoints() {
		return currentMovePoints;
	}

	public int getRankPoints() {
		return rankPoints;
	}
	
	public int wealth(){
		return gold;
	}
	
	public void earnGold(int earnings){
		if(earnings < 0) return;
		this.gold += earnings;
	}
	
	public void loseGold(int loss){
		if(loss < 0) return;
		this.gold -= loss;
	}
	
	public Pawn getActivePawn() {
		if(activePawn < 0){
			return null;
		}
		return heroesOwned.get(activePawn);
	}
	
	public void activateNextPawn() {
		if(++this.activePawn > heroesOwned.size() - 1){
			this.activePawn = 0;
		}
	}
	
	public List<? extends Hero> getAllPawns(){
		return heroesOwned;
	}
}