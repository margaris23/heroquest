package com.nima;

import java.util.List;

public abstract class Player {
	public static final String XARGON = "Xargon";
	public enum Action {
		ATTACK, CAST_SPELL, SEARCH_TREASURE, SEARCH_SECRET,
		SEARCH_TRAP, DISARM_TRAP, NONE
	}
	
	protected Action currentAction = Action.NONE;
	protected int activePawn = -1;
	protected String name;
	
	public Action getCurrentAction() {
		return currentAction;
	}
	
	public final String getName(){
		return name;
	}
	
	public final boolean isXargon(){
		return XARGON.equals(name);
	}
	/*
	 * Abstract Section
	 */
	public abstract Pawn getActivePawn();
	public abstract void activateNextPawn();
	public abstract List<? extends Pawn> getAllPawns();
}
