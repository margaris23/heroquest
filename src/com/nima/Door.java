package com.nima;

public class Door {
	public enum ORIENTATION {
		WEST, NORTH, EAST, SOUTH
	}	
	public enum TYPE {
		HIDDEN, VISIBLE, FAKE
	}
	
	private ORIENTATION orientation;
	private TYPE type;
	private boolean opened;
	
	public ORIENTATION getOrientation() {
		return orientation;
	}
	public void setOrientation(ORIENTATION orientation) {
		this.orientation = orientation;
	}
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}
	public boolean isOpened() {
		return opened;
	}
	public void setOpened(boolean opened) {
		this.opened = opened;
	}
	
}
