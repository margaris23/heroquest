package com.nima;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import com.nima.Pawn.STATE;

public class HeroQuest {
	public static void main(String[] args) {
		System.out.println("Welcome to HeroQuest Engine!!!");
				
		
		//read configuration file		
		Properties config = getProperties("src/com/nima/conf.hq");
		
		try {
			PrintStream ps = new PrintStream(System.out, true, "UTF-8");
			ps.println(config.getProperty("board.default"));
		} catch (UnsupportedEncodingException e) {			
			e.printStackTrace();
		}
		
		System.out.println();
		
		//create new game
		int numberOfPlayers = 1;
		Game game = new Game(numberOfPlayers);
		HumanPlayer player;
		
		//Prepare Board with a quest
		try{			
			game.startQuest(getProperties("src/com/nima/quest1.hq"));
			
			//create player
			player = new HumanPlayer(getProperties("src/com/nima/player.hq"));
			
			// add player to game
			game.playerJoined(player);
			
			System.out.println(game.getBoard());
			
			Pawn pawn = player.getActivePawn();
			//position hero to board
			pawn.setPosx(12);
			pawn.setPosy(0);
			
			// Need to write down what each class should do from now on ... !!!
			
			//define turns
			game.rollTurns();
			
			//simple logic to be implemented and tested
			
			game.begin();
			
			// check if game ended
			while(!game.isGameEnded()){				
			
				// who is next?
				Player currentPlayer = game.next();
				if(Player.XARGON.equals(currentPlayer.getName())){
					/* DM's play
					 * 1) find active monsters
					 * 2) decide monster's turn
					 * 3) find next monster's visible heroes
					 * 4) move monster to closest hero
					 * 5) if not close then goto :7
					 * 6) attack adjacent hero
					 * 7) if monsters available then goto :3 
					 * */					
				}else{					
					//check if next player is alive: if yes then 'play'
					if(currentPlayer.getActivePawn().getCurrentState() != STATE.DEAD){
						
						currentPlayer.getActivePawn().rollMovement();
						
						game.showMoveMap();
						
						boolean validMove = currentPlayer.getActivePawn().move(0, 0);
						
						game.searchTreasure(currentPlayer.getActivePawn());
						game.searchSecret(currentPlayer.getActivePawn());
						game.searchTrap(currentPlayer.getActivePawn());
						game.disarmTrap(currentPlayer.getActivePawn());
						
						currentPlayer.getActivePawn().attack(null);						
					}
				}
							
				game.end();
				game.endCurrentTurn();
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}		
		
		System.out.println("Finished!!!");
	}
	
	private static void cmdtest(Game game){
		InputStreamReader isr = new InputStreamReader(System.in);
		int input = ' ';
		
		while(input != 'x'){
			showMenu();
			try {
				input = isr.read();
			
			
			switch(input){
			case 'b':
				System.out.println(game.getBoard());
				break;
			case 'a':
				break;
			case 's':
				break;
			case 'd':
				break;
			case 'w':
				break;
			case 'i':
				break;
			case 'j':
				break;
			case 'k':
				break;
			case 'l':
				break;
			case 'r':					
				break;
				
			}
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
			
	}
	
	private static void showMenu(){
		System.out.println("Menu\n====\n\nx: Exit\nb: Show Board\nr: Roll Move Dice\nw: Move Up\ns: Move Down\na: Move Left\nd: Move Right\ni: Attack Up\nj: Attack Left\nk: Attack Down\nl: Attack Right\n");
		
	}
	
	private static Properties getProperties(String fileName){
		Properties props = new Properties();
		File file = new File(fileName);
		try {			
			FileInputStream fis = new FileInputStream(file);
			props.load(new InputStreamReader(fis, "UTF-8"));			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return props;
	}
}