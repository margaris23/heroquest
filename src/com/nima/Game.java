package com.nima;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class Game {
	
	/*
	 * Must:
	 * 		keep game's current status
	 * 		keep track of active pawn
	 * 		keep track of who's is active pawn
	 */
	
	private List<Player> players = new ArrayList<Player>();
	private int numberOfPlayers;
	private Board board = new Board();
	private Quest quest;	
	
	private int currentPlayerIndex;
	private int currentTurn = 0;
	private boolean gameEnded = false;
	private int maxNumberOfHeroes;
	
	public Game(int numberOfPlayers){
		this.numberOfPlayers = numberOfPlayers;
		playerJoined(new Xargon());	// ... add the evil guy!!!
	}
	
	public void startQuest(Properties questProps) throws Exception{
		
		/* Load Quest from file properties */
		this.quest = new Quest(questProps.getProperty("name"), questProps.getProperty("map"),
				questProps.getProperty("text"), questProps.getProperty("notes"));
		
		/* Create map */
		this.board.configure(questProps.getProperty("map").toCharArray());
		
		/* Create doors */
		this.board.setupDoors(questProps.getProperty("doors"));
		
		/* Create rest of data */
		this.maxNumberOfHeroes = Integer.valueOf(questProps.getProperty("max_heroes"));
	}
	
	public int getMaxNumberOfHeroes(){
		return maxNumberOfHeroes;
	}
	
	public void playerJoined(Player player){
		this.players.add(player);
	}
	
	public void begin(){
		//for now traverse through the array of players as is
		this.currentPlayerIndex = 0;
		this.players.get(0).activateNextPawn();
	}
	
	public Pawn getCurrentPawn(){
		return pawnQueue.get(0);
	}
	
	public void end(){
		this.gameEnded = true;
	}
	
	public boolean isGameEnded(){
		return gameEnded;
	}
	
	public Player next(){
		return players.get(currentPlayerIndex);
	}
	
	public void endCurrentTurn(){
		this.currentTurn += 1;
	}
	
	public int getCurrentTurn(){
		return currentTurn;
	}

	public String getBoard(){
		return board.export();
	}
	
	List<Pawn> pawnQueue = new ArrayList<Pawn>();
	public void rollTurns(){
		pawnQueue.clear();
		for(int i = 0; i < numberOfPlayers; i++){						
			pawnQueue.addAll(players.get(i).getAllPawns());
		}
		Collections.shuffle(pawnQueue);		
	}
	
	public void showMoveMap(){		
	}
	
	public void searchTreasure(Pawn pawn){		
	}
	
	public void searchSecret(Pawn pawn){		
	}
	
	public void searchTrap(Pawn pawn){		
	}
	
	public void disarmTrap(Pawn pawn){		
	}
}
